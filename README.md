# merchandise_ui_service

## Guide

- Install packages

```
yarn install
```

- Put env variables to .env file

- Run dev server

```
yarn start
```

- Build

```
yarn build
```
