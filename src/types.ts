export interface ReceiverAddress {
  fullname: string;
  mobile: string;
  address: string;
  province: string;
  district: string;
  note?: string;
}

export interface Voucher {
  voucherCode: string;
  rewardName: string;
  offerId: string;
  brandCode: string;
  status: string;
  images?: string;
  startTime?: string;
  endTime?: string;
}

export interface Dist {
  name: string;
  type: string;
  slug: string;
  name_with_type: string;
  path: string;
  path_with_type: string;
  code: string;
  parent_code: string;
}

export interface Province {
  name: string;
  slug: string;
  type: string;
  name_with_type: string;
  code: string;
}
