export const isEmpty = (value: any) => {
  return (
    value === null ||
    value === "" ||
    value === undefined ||
    Object.keys(value).length === 0 ||
    value.length === 0
  );
};

export const stringToASCII = (str: string) => {
  try {
    return str
      .toLowerCase()
      .replace(/[àáảãạâầấẩẫậăằắẳẵặ]/g, "a")
      .replace(/[èéẻẽẹêềếểễệ]/g, "e")
      .replace(/[đ]/g, "d")
      .replace(/[ìíỉĩị]/g, "i")
      .replace(/[òóỏõọôồốổỗộơờớởỡợ]/g, "o")
      .replace(/[ùúủũụưừứửữự]/g, "u")
      .replace(/[ỳýỷỹỵ]/g, "y");
  } catch {
    return "";
  }
};
