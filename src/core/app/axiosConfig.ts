import axios from "axios";

const axiosConfig = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
});
//process.env.REACT_APP_API_URL
axiosConfig.defaults.headers.post["Content-Type"] = "application/json";
axiosConfig.interceptors.request.use(
  (config) => {
    // ** Get token from localStorage

    const token = localStorage.getItem("token");

    // ** If token is present add it to request's Authorization Header

    if (token) {
      // ** eslint-disable-next-line no-param-reassign
      if (config.headers) {
        config.headers.Authorization = `Bearer ${token}`;
      }
    }

    return config;
  },

  (error) => Promise.reject(error)
);
export default axiosConfig;
