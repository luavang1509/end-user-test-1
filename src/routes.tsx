import { createBrowserRouter } from "react-router-dom";
import App from "./App";
import ConfirmedPage from "./pages/ConfirmedPage";
import ErrorPage from "./pages/ErrorPage";
import FormPage from "./pages/FormPage";
import HistoryPage from "./pages/HistoryPage";
import HomePage from "./pages/HomePage";
import TokenPage from "./pages/TokenPage";

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "form",
        element: <FormPage />,
      },
      {
        path: "confirmed",
        element: <ConfirmedPage />,
      },
      {
        path: "history",
        element: <HistoryPage />,
      },
      {
        path: "/",
        element: <HomePage />,
      },
      {
        path: "/:token",
        element: <TokenPage />,
      },
      {
        path: "/error",
        element: <ErrorPage />,
      },
    ],
  },
]);

export default router;
