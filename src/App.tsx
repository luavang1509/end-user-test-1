import { useEffect } from "react";
import { Outlet, useLocation } from "react-router-dom";

function App() {
  let location = useLocation();

  useEffect(() => {
    setTimeout(() => {
      window.scrollTo(0, 0);
    }, 0);
  }, [location]);

  return (
    <div className="wrapper">
      <Outlet />
    </div>
  );
}

export default App;
