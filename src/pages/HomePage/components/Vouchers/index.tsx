import { FrownOutlined } from "@ant-design/icons";
import "moment/locale/vi";
import { Key, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { motion } from "framer-motion";
import { Checkbox, notification } from "antd";
import axiosConfig from "../../../../core/app/axiosConfig";
import { useAppDispatch } from "../../../../core/app/hooks";
import meditateMascot from "../../../../core/assets/images/meditate-mascot.png";
import defaultMerchandise from "../../../../core/assets/images/default-merchandise.png";
import { Voucher } from "../../../../types";
import { isEmpty } from "../../../../utils";
import { checkVouchers } from "../../../FormPage/formSlice";
import { CheckboxChangeEvent } from "antd/lib/checkbox";

import "./Vouchers.scss";
import LoadingMascot from "../../../../components/LoadingMascot";

type Props = {};

const Vouchers = (props: Props) => {
  const [vouchers, setVouchers] = useState<Voucher[]>([]);
  const [checkedVouchers, setCheckedVouchers] = useState<Voucher[]>([]);
  const [checkedAll, setCheckedAll] = useState(false);
  const [loading, setLoading] = useState(true);

  //Redux
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  useEffect(() => {
    const controller = new AbortController();
    setLoading(true);

    const getProducts = async () => {
      await axiosConfig
        .get(`/merchandise/my-coupon`)
        .then((response) => {
          setVouchers(response?.data?.data);
          setLoading(false);
        })
        .catch((err) => {
          if (err) {
            setLoading(false);
          }
        });
    };
    getProducts();

    return () => {
      controller.abort();
      setLoading(false);
    };
  }, []);

  const handleCheck = (e: CheckboxChangeEvent, voucher: Voucher) => {
    const checked = e.target.checked;
    const newChecked = [...checkedVouchers];
    if (checked) {
      newChecked.push(voucher);
      newChecked?.length === vouchers?.length && setCheckedAll(true);
    } else {
      setCheckedAll(false);
      newChecked.splice(checkedVouchers.indexOf(voucher), 1);
    }
    setCheckedVouchers(newChecked);
  };

  const onCheckAllChange = (e: CheckboxChangeEvent) => {
    const checked = e.target.checked;
    if (checked) {
      setCheckedVouchers(vouchers);
      setCheckedAll(true);
    } else {
      setCheckedVouchers([]);
      setCheckedAll(false);
    }
  };

  const handleRedeem = () => {
    try {
      dispatch(checkVouchers(checkedVouchers));
      navigate("/form");
    } catch (error) {
      notification.error({
        message: "Có gì đó sai sai",
        description: "Bạn thử lại sau nhen",
        icon: <FrownOutlined style={{ color: "#f65d79" }} />,
        duration: 2,
      });
    }
  };
  return (
    <div className="pageContent-container">
      {loading && <LoadingMascot />}
      {!loading && !isEmpty(vouchers) && (
        <>
          <motion.div initial={{ y: -30 }} animate={{ y: 0 }}>
            <div className="step-hero">Bước 1: Chọn phần quà muốn giao</div>
            <div className="container mb-4">
              <div className="step-title">
                <h1 className="step-title_text">
                  <span>Quà đang có</span>
                  <strong>[{vouchers.length}]</strong>
                </h1>
                <Checkbox
                  checked={checkedAll}
                  onChange={(e) => onCheckAllChange(e)}
                  className="f-center step-title_selectAll clickable-text-secondary"
                >
                  <div>{checkedAll ? "Bỏ chọn tất cả" : "Chọn tất cả"}</div>
                </Checkbox>
              </div>

              {vouchers?.map((voucher) => {
                const { rewardName, voucherCode, images } = voucher;

                return (
                  <div key={voucherCode as Key} className="vouchers-container">
                    <Checkbox
                      className="voucher-info"
                      checked={checkedVouchers?.includes(voucher)}
                      style={{
                        background: checkedVouchers?.includes(voucher)
                          ? "rgba(246, 93, 121, 0.1)"
                          : "",
                      }}
                      onChange={(e) => handleCheck(e, voucher)}
                    >
                      <img
                        className="voucher-info_img"
                        src={images ? images[0] : defaultMerchandise}
                        alt=""
                      />
                      <div className="voucher-info_text">
                        <p className="voucher-info_text-name">{rewardName}</p>
                        <p className="voucher-info_text-code">
                          Mã: {voucherCode}
                        </p>
                      </div>
                    </Checkbox>
                  </div>
                );
              })}
            </div>
          </motion.div>
          <div className="container bottom-fixed">
            <button
              onClick={handleRedeem}
              className="primary-btn redeem-btn"
              disabled={checkedVouchers?.length === 0}
            >
              Chọn &nbsp;
              {`${
                checkedVouchers.length !== 0
                  ? `[${checkedVouchers.length}]`
                  : ""
              }`}
            </button>
          </div>
        </>
      )}
      {!loading && isEmpty(vouchers) && (
        <div className="noVoucher-container">
          <img src={meditateMascot} alt="" />
          <div className="noVoucher-text mb-1">
            <p className="mb-1">Không có phần quà nào chưa giao.</p>
          </div>
        </div>
      )}
    </div>
  );
};

export default Vouchers;
