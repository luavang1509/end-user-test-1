import { useNavigate } from "react-router-dom";
import Vouchers from "./components/Vouchers";
import React from "react";

import "./index.scss";

type Props = {};

const HomePage = (props: Props) => {
  const navigate = useNavigate();

  return (
    <>
      <div className="page-title top-fixed">
        <span>Thông tin giao quà</span>
        <div
          onClick={() => navigate("/history")}
          className="page-title_history"
        >
          Lịch sử
        </div>
      </div>
      <Vouchers />
    </>
  );
};

export default HomePage;
