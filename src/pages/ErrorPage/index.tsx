import "./ErrorPage.scss";
import meditateMascot from "../../core/assets/images/meditate-mascot.png";
import { useNavigate } from "react-router-dom";

export default function ErrorPage() {
  const navigate = useNavigate();

  return (
    <div className="wrapper">
      <div className="errorPage">
        <img className="errorPage-img" src={meditateMascot} alt="" />
        <p className="errorPage-text">Hông có gì ở đây hết trơn.</p>
        <button
          onClick={() => navigate("/")}
          className="primary-btn errorPage-btn"
        >
          Về trang chủ
        </button>
      </div>
    </div>
  );
}
