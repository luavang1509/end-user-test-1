import { useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import axiosConfig from "../../core/app/axiosConfig";

type Props = {};

const TokenPage = (props: Props) => {
  const navigate = useNavigate();
  const { token } = useParams();
  useEffect(() => {
    if (token) {
      axiosConfig
        .post("/auth/sign-in-token", {
          token,
        })
        .then((res) => {
          localStorage.setItem("token", res.data.data.token);
          //redirect to home
          navigate("/");
        })
        .catch((err) => {
          console.log(err);
          navigate("/error");
        });
    }
  }, []);
  return <></>;
};

export default TokenPage;
