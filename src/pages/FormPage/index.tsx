import InfoForm from "./components/InfoForm";
import { motion } from "framer-motion";
import { useNavigate } from "react-router-dom";
import backIcon from "../../core/assets/icons/back-icon.svg";

type Props = {};

const FormPage = (props: Props) => {
  const navigate = useNavigate();
  return (
    <motion.div>
      <div className="page-title has-back top-fixed">
        <img onClick={() => navigate(-1)} src={backIcon} alt="" />
        Xác nhận giao hàng
      </div>
      <InfoForm />
    </motion.div>
  );
};

export default FormPage;
