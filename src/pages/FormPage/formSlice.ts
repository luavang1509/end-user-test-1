import { createSlice } from "@reduxjs/toolkit";
import { Voucher } from "../../types";

interface InitialState {
  checked: Array<Voucher>;
}

const initialState: InitialState = {
  checked: [],
};

const formSlice = createSlice({
  name: "form",
  initialState,
  reducers: {
    checkVouchers: (state, action) => {
      state.checked = action.payload;
    },
  },
});

export const { checkVouchers } = formSlice.actions;
export default formSlice.reducer;
