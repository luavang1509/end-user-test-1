import { useState } from "react";
import { useNavigate } from "react-router-dom";
import axiosConfig from "../../../../core/app/axiosConfig";
import { useAppDispatch, useAppSelector } from "../../../../core/app/hooks";
import { checkVouchers } from "../../formSlice";

import ErrorModal from "../ErrorModal";
import CustomModal from "../../../../components/CustomModal";
import LoadingModal from "../../../../components/LoadingModal";
import "./ConfirmModal.scss";

type Props = {
  isModalOpen: boolean;
  setIsModalOpen: Function;
  formValues: any;
};

const ConfirmModal = (props: Props) => {
  const { isModalOpen, setIsModalOpen, formValues } = props;
  const navigate = useNavigate();

  //Redux
  const dispatch = useAppDispatch();
  const checked = useAppSelector((state) => state.form.checked);

  const [submitting, setSubmitting] = useState(false);

  const [isErrModalOpen, setIsErrModalOpen] = useState(false);

  const showErrorModal = () => setIsErrModalOpen(true);

  const hideModal = () => {
    setIsModalOpen(false);
  };

  const handleModalCancel = () => {
    hideModal();
  };

  const handleSubmit = async () => {
    try {
      setSubmitting(true);

      await axiosConfig
        .post("/order", {
          listMerchandise: checked.map((merchandise) => {
            const { voucherCode, rewardName, offerId, brandCode } = merchandise;
            return {
              voucherCode,
              rewardName,
              offerId,
              brandCode,
            };
          }),
          receiverAddress: {
            address: formValues?.address,
            district: formValues?.district?.split(",")[0],
            province: formValues?.province?.split(",")[0],
            mobile: formValues?.mobile,
            fullname: formValues?.fullname,
          },
          note: formValues?.note,
        })
        .then(() => {
          dispatch(checkVouchers([]));
          setSubmitting(false);
          navigate("/confirmed");
        });
    } catch (error) {
      console.log(error);
      setSubmitting(false);
      hideModal();
      showErrorModal();
    }
  };

  return (
    <>
      {!submitting ? (
        <CustomModal isModalOpen={isModalOpen} setIsModalOpen={setIsModalOpen}>
          <div className="confirmModal">
            <h1 className="confirmModal-header">Xác nhận</h1>
            <p className="confirmModal-content">
              Bạn chắc chắn với những thông tin nhận hàng này chứ?
            </p>
            <div className="confirmModal-btnGroup">
              <button
                key="back"
                className="secondary-btn confirmModal-btn"
                onClick={handleModalCancel}
              >
                Quay lại
              </button>
              <button
                key="submit"
                className="primary-btn confirmModal-btn"
                onClick={handleSubmit}
              >
                Xác nhận
              </button>
            </div>
          </div>
        </CustomModal>
      ) : (
        <LoadingModal
          isLoadingModalOpen={submitting}
          setIsLoadingModalOpen={setSubmitting}
        />
      )}

      <ErrorModal
        isErrModalOpen={isErrModalOpen}
        setIsErrModalOpen={setIsErrModalOpen}
      />
    </>
  );
};

export default ConfirmModal;
