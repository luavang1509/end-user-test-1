import errorMascot from "../../../../core/assets/images/Error.png";
import React from "react";

import "./ErrorModal.scss";
import CustomModal from "../../../../components/CustomModal";

type Props = {
  isErrModalOpen: boolean;
  setIsErrModalOpen: any;
};

const ErrorModal = (props: Props) => {
  const { isErrModalOpen, setIsErrModalOpen } = props;
  const hideErrorModal = () => setIsErrModalOpen(false);

  return (
    <CustomModal
      isModalOpen={isErrModalOpen}
      setIsModalOpen={setIsErrModalOpen}
      style={{ maxWidth: "34.3rem" }}
    >
      <img className="errorModal-img" src={errorMascot} alt="" />
      <div className="errorModal">
        <h1 className="errorModal-header">Chưa xác nhận được</h1>
        <p className="errorModal-content">Uống miếng nước và thử lại nha.</p>
        <button
          onClick={hideErrorModal}
          key="submit"
          className="primary-btn errorModal-btn"
        >
          OK
        </button>
      </div>
    </CustomModal>
  );
};

export default ErrorModal;
