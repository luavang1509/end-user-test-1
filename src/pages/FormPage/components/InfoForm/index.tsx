import { Form, Input, Select } from "antd";
import { motion } from "framer-motion";
import { useEffect, useState } from "react";

import TextArea from "antd/lib/input/TextArea";
import { SelectValue } from "antd/lib/select";
import { cities as citiesData } from "../../../../constants/cities";
import { dists as distsData } from "../../../../constants/dist";
import axiosConfig from "../../../../core/app/axiosConfig";
import { Dist, Province, ReceiverAddress } from "../../../../types";
import ConfirmModal from "../ConfirmModal";

import { useNavigate } from "react-router-dom";
import LoadingMascot from "../../../../components/LoadingMascot";
import { customNotification } from "../../../../components/notification";
import VouchersList from "../../../../components/VouchersList";
import { useAppSelector } from "../../../../core/app/hooks";
import { isEmpty, stringToASCII } from "../../../../utils";
import "./InfoForm.scss";

type Props = {};

const validationErrors = {
  empty: "Oops! Đừng để trống ô này nhé.",
};

const InfoForm = (props: Props) => {
  const [form] = Form.useForm();

  //Redux
  const checkedVouchers = useAppSelector((state) => state.form.checked);
  const navigate = useNavigate();

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    checkedVouchers.length === 0 && navigate("/");
  }, [checkedVouchers.length, navigate]);

  const [userOldAddress, setUserOldAddress] = useState<ReceiverAddress>();
  const [cities] = useState<Province[]>(Object.values(citiesData));
  const [dists, setDists] = useState<Dist[]>(Object.values(distsData));

  const [fields, setFields] = useState<ReceiverAddress>({
    fullname: "",
    address: "",
    district: "",
    mobile: "",
    province: "",
    note: "",
  });

  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };

  useEffect(() => {
    const controller = new AbortController();
    const getUserOldAddress = async () => {
      setLoading(true);
      try {
        await axiosConfig
          .get(`/address/my-address`, { signal: controller.signal })
          .then((res) => {
            setUserOldAddress(res?.data?.addressDetail);
            setLoading(false);
          });
      } catch (error) {
        setLoading(false);
      }
    };
    getUserOldAddress();

    return () => controller.abort();
  }, []);

  useEffect(() => {
    form.resetFields();
  }, [userOldAddress]);

  const handleProvinceChange = (value: SelectValue) => {
    form.setFieldValue("dist", "");
    setFields({ ...fields, district: "" });
    setFields({ ...fields, province: value?.toString().split(",")[0] || "" });

    const code = value?.toString().split(",")[1];
    const newDists: Dist[] = [];
    Object.values(distsData).forEach((dist) => {
      if (dist.parent_code === code) newDists.push(dist);
    });
    setDists(newDists);
  };

  const handleDistChange = (value: SelectValue) => {
    const parent_code = value?.toString().split(",")[1];
    if (form.getFieldValue("province") === undefined) {
      let provinceOfChosenDist: string = "";
      Object.values(citiesData).every((province) => {
        if (province.code === parent_code) {
          provinceOfChosenDist = province.name_with_type;
          return false;
        }
        return true;
      });
      form.setFieldValue("province", provinceOfChosenDist);
      setFields({ ...fields, province: provinceOfChosenDist });
    }
    form.setFieldValue("dist", value?.toString().split(",")[0] || "");
    setFields({ ...fields, district: value?.toString().split(",")[0] || "" });
  };

  const handleValidate = async () => {
    try {
      await form.validateFields();
      showModal();
    } catch (error: any) {
      error?.errorFields?.every((errorField: any) => {
        if (errorField.errors[0] === validationErrors.empty) {
          customNotification("Chưa điền đủ thông tin. Bổ sung ngay.");
          return false;
        } else {
          customNotification("Chưa xác nhận được. Thử lại nha.");
          return false;
        }
      });
    }
  };

  return (
    <div className="pageContent-container">
      {(loading || isEmpty(userOldAddress)) && <LoadingMascot />}
      {!loading && !isEmpty(userOldAddress) && (
        <motion.div initial={{ y: -30 }} animate={{ y: 0 }} className="mb-4">
          <div className="step-hero">Bước 2: Điền thông tin</div>

          <div className="container mb-1">
            <div className="title mb-2">
              <span>Thông tin người nhận</span>
            </div>
            <div className="infoForm">
              <Form
                layout="vertical"
                form={form}
                initialValues={userOldAddress}
              >
                <Form.Item
                  label="Tên người nhận"
                  name="fullname"
                  rules={[{ required: true, message: validationErrors.empty }]}
                >
                  <Input
                    onChange={(e) =>
                      setFields({ ...fields, fullname: e.target.value })
                    }
                    placeholder="Nhập tên người nhận"
                  />
                </Form.Item>

                <Form.Item
                  label="Số điện thoại người nhận"
                  name="mobile"
                  rules={[
                    { required: true, message: validationErrors.empty },
                    {
                      validator: (_, value) => {
                        if (!value) return;

                        if (value.length === 10) {
                          return Promise.resolve();
                        }

                        return Promise.reject(
                          new Error(
                            "Oops! Số điện thoại này không hợp lệ. Bạn kiểm tra lại nha. "
                          )
                        );
                      },
                    },
                  ]}
                >
                  <Input
                    type="number"
                    onChange={(e) =>
                      setFields({ ...fields, mobile: e.target.value })
                    }
                    placeholder="Nhập số điện thoại người nhận"
                  />
                </Form.Item>

                <Form.Item
                  label="Tỉnh/ Thành Phố"
                  name="province"
                  rules={[{ required: true, message: validationErrors.empty }]}
                >
                  <Select
                    showSearch
                    onChange={handleProvinceChange}
                    placeholder="Nhập Tỉnh/ Thành Phố"
                    notFoundContent="Không tìm thấy địa điểm này"
                  >
                    {Object.values(cities).map((province) => {
                      const { name_with_type, code } = province;
                      return (
                        <Select.Option
                          key={province.code}
                          value={`${name_with_type},${code},${stringToASCII(
                            name_with_type
                          )}`}
                        >
                          {name_with_type}
                        </Select.Option>
                      );
                    })}
                  </Select>
                </Form.Item>

                <Form.Item
                  label="Quận/ Huyện"
                  name="district"
                  rules={[{ required: true, message: validationErrors.empty }]}
                >
                  <Select
                    showSearch
                    onChange={handleDistChange}
                    placeholder="Nhập Quận/ Huyện"
                    notFoundContent="Không tìm thấy địa điểm này"
                  >
                    {Object.values(dists).map((dist) => {
                      const { code, name_with_type, parent_code } = dist;
                      return (
                        <Select.Option
                          key={code}
                          value={`${name_with_type},${parent_code},${stringToASCII(
                            name_with_type
                          )}`}
                        >
                          {name_with_type}
                        </Select.Option>
                      );
                    })}
                  </Select>
                </Form.Item>

                <Form.Item
                  label="Địa chỉ nhận hàng"
                  name="address"
                  rules={[{ required: true, message: validationErrors.empty }]}
                >
                  <Input
                    onChange={(e) =>
                      setFields({ ...fields, address: e.target.value })
                    }
                    placeholder="Nhập địa chỉ nhận hàng"
                  />
                </Form.Item>

                <Form.Item label="Ghi chú" name="note">
                  <TextArea
                    onChange={(e) =>
                      setFields({ ...fields, note: e.target.value })
                    }
                    placeholder="Nhập ghi chú cho TAPTAP"
                  />
                </Form.Item>
              </Form>
            </div>
          </div>

          <VouchersList vouchers={checkedVouchers} />

          <div className="infoForm-alert">
            <span>Lưu ý:</span>
            <li>
              TAPTAP sẽ tổng hợp thông tin và chuyển quà đi vào mỗi thứ 6 hàng
              tuần.
            </li>
            <li>
              Thời gian vận chuyển từ 2-7 ngày làm việc (tuỳ vào điều kiện thực
              tế).
            </li>
          </div>

          <div className="container bottom-fixed">
            <button
              onClick={handleValidate}
              className="primary-btn infoForm_submit-btn"
            >
              Xác nhận
            </button>
          </div>

          <ConfirmModal
            formValues={form.getFieldsValue()}
            isModalOpen={isModalOpen}
            setIsModalOpen={setIsModalOpen}
          />
        </motion.div>
      )}
    </div>
  );
};

export default InfoForm;
