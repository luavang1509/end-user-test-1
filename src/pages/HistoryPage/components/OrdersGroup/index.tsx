import { motion } from "framer-motion";
import { Divider } from "antd";

import "./OrdersGroup.scss";
import VouchersList from "../../../../components/VouchersList";
import backIcon from "../../../../core/assets/icons/back-icon.svg";
import { useEffect } from "react";
import { isEmpty } from "../../../../utils";

type Props = {
  openedOrdersGroup: any[];
  setOpenedOrdersGroup: any;
};

const OrdersGroup = (props: Props) => {
  const { openedOrdersGroup, setOpenedOrdersGroup } = props;

  const { note, receiverAddress } = openedOrdersGroup[0];
  const { address, district, province, fullname, mobile } = receiverAddress;

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      {!isEmpty(openedOrdersGroup) && (
        <div
          className="pageContent-container"
          style={{ marginTop: "7rem", paddingBottom: "5.4rem" }}
        >
          <div className="history-title page-title has-back top-fixed">
            <img
              onClick={() => setOpenedOrdersGroup([])}
              src={backIcon}
              alt=""
            />
            Chi tiết đơn hàng
          </div>
          <motion.div
            initial={{ x: -50 }}
            animate={{ x: 0 }}
            exit={{ opacity: 0 }}
            className="pageContent-container"
          >
            <div className="receive-detail-container container">
              <div className="title mb-1">Thông tin nhận hàng</div>
              <div className="receive-detail">
                <p className="receive-detail_address">
                  {address}, {district}, {province}
                </p>
                <Divider />
                <p className="receive-detail_fullname">{fullname}</p>
                <p className="receive-detail_mobile">{mobile}</p>
                {note && (
                  <>
                    <Divider />
                    <p className="receive-detail_note">{note}</p>
                  </>
                )}
              </div>
            </div>

            <VouchersList
              vouchers={openedOrdersGroup.map((openedOrder) => {
                return openedOrder.merchandise;
              })}
            />

            <div
              style={{ padding: "1.2rem 1.6rem 0" }}
              className="infoForm-alert"
            >
              <span>Lưu ý:</span>
              <li>
                TAPTAP sẽ tổng hợp thông tin và chuyển quà đi vào mỗi thứ 6 hàng
                tuần.
              </li>
              <li>
                Thời gian vận chuyển từ 2-7 ngày làm việc (tuỳ vào điều kiện
                thực tế).
              </li>
            </div>
          </motion.div>

          <div className="container callTAPTAP-container bottom-fixed">
            <p className="callTAPTAP-text">
              Nếu có thay đổi về thông tin giao hàng, vui lòng liên hệ TAPTAP
              ngay
            </p>

            <a href="tel:+842873046446" className="primary-btn callTAPTAP-btn">
              <span>Gọi TAPTAP ngay</span>
            </a>
          </div>
        </div>
      )}
    </>
  );
};

export default OrdersGroup;
