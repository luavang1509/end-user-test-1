import moment from "moment";
import defaultMerchandise from "../../../../core/assets/images/default-merchandise.png";
import { RightOutlined } from "@ant-design/icons";

import "./OrdersGroups.scss";

type Props = {
  ordersGroups: any[];
  setOpenedOrdersGroup: any;
};

const OrdersGroups = (props: Props) => {
  const { ordersGroups, setOpenedOrdersGroup } = props;

  return (
    <div
      className="pageContent-container"
      style={{ marginTop: "7rem", paddingBottom: "14.8rem" }}
    >
      {ordersGroups?.map((ordersGroup, index) => {
        const { submitDate, merchandise, images } = ordersGroup[0];
        const { rewardName } = merchandise;

        const handleMoreClick = () => {
          setOpenedOrdersGroup(ordersGroup);
        };

        return (
          <div key={index} className="ordersGroup-container container">
            <div className="ordersGroup-date">
              {moment(submitDate).format("hh:mm A · DD/MM/YYYY")}
            </div>
            <div className="ordersGroup-firstReward">
              <img
                className="ordersGroup-firstReward_img"
                src={images ? images[0] : defaultMerchandise}
                alt=""
              />
              <p className="ordersGroup-firstReward_name">{rewardName}</p>
            </div>
            <div className="ordersGroup-footer">
              <span className="ordersGroup-footer_count">
                {ordersGroup?.length} phần quà
              </span>
              <div
                className="ordersGroup-footer_more"
                onClick={handleMoreClick}
              >
                <span>Xem thêm quà</span>
                <RightOutlined style={{ fontSize: "12px" }} />
              </div>
            </div>
          </div>
        );
      })}

      <div className="container callTAPTAP-container bottom-fixed">
        <p className="callTAPTAP-text">
          Nếu có thay đổi về thông tin giao hàng, vui lòng liên hệ TAPTAP ngay
        </p>

        <a href="tel:+842873046446" className="primary-btn callTAPTAP-btn">
          <span>Gọi TAPTAP ngay</span>
        </a>
      </div>
    </div>
  );
};

export default OrdersGroups;
