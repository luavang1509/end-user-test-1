import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { motion } from "framer-motion";
import moment from "moment";
import "moment/locale/es";
import axiosConfig from "../../core/app/axiosConfig";
import walkingMascot from "../../core/assets/images/walking-mascot.png";
import meditateMascot from "../../core/assets/images/meditate-mascot.png";
import backIcon from "../../core/assets/icons/back-icon.svg";

import { isEmpty } from "../../utils";

import "./HistoryPage.scss";
import OrdersGroups from "./components/OrdersGroups";
import OrdersGroup from "./components/OrdersGroup";

type Props = {};

const HistoryPage = (props: Props) => {
  moment.locale("es");

  const navigate = useNavigate();
  const [loading, setLoading] = useState(true);
  const [ordersGroups, setOrdersGroups] = useState([]);
  const [openedOrdersGroup, setOpenedOrdersGroup] = useState([]);

  useEffect(() => {
    const controller = new AbortController();
    const getHistoryRewards = async () => {
      setLoading(true);
      try {
        await axiosConfig
          .get(`/order/my-order`, { signal: controller.signal })
          .then((res) => {
            setOrdersGroups(res?.data);
            setLoading(false);
          });
      } catch (error) {
        setLoading(false);
      }
    };
    getHistoryRewards();

    return () => controller.abort();
  }, []);

  return (
    <>
      {isEmpty(openedOrdersGroup) && (
        <div className="history-title page-title has-back top-fixed">
          <img onClick={() => navigate(-1)} src={backIcon} alt="" />
          Lịch sử giao hàng
        </div>
      )}
      <div>
        {loading && (
          <div className="loading-screen">
            <img src={walkingMascot} alt="" />
            <p>Chờ xíu nha...</p>
          </div>
        )}

        {/* Loading stopped and ordersGroups is not empty */}
        {!loading &&
          !isEmpty(ordersGroups) &&
          // Check whether ordersGroup is opened?
          (isEmpty(openedOrdersGroup) ? (
            <motion.div initial={{ y: -50 }} animate={{ y: 0 }}>
              <OrdersGroups
                ordersGroups={ordersGroups}
                setOpenedOrdersGroup={setOpenedOrdersGroup}
              />
            </motion.div>
          ) : (
            <OrdersGroup
              openedOrdersGroup={openedOrdersGroup}
              setOpenedOrdersGroup={setOpenedOrdersGroup}
            />
          ))}

        {!loading && isEmpty(ordersGroups) && (
          <>
            <div className="noVoucher-container">
              <img src={meditateMascot} alt="" />
              <div className="noVoucher-text mb-1">
                <p>Chưa có đơn hàng nào</p>
              </div>
            </div>
            <div className="container">
              <button disabled className="primary-btn callTAPTAP-btn">
                Xác nhận
              </button>
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default HistoryPage;
