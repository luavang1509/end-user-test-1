import { motion } from "framer-motion";
import { useNavigate } from "react-router-dom";
import yayMascot from "../../core/assets/images/yay-balloon.png";
import backIcon from "../../core/assets/icons/back-icon.svg";

import "./ConfirmedPage.scss";

type Props = {};

const ConfirmedPage = (props: Props) => {
  const navigate = useNavigate();
  return (
    <>
      <div className="page-title has-back top-fixed">
        <img onClick={() => navigate("/")} src={backIcon} alt="" />
        Xác nhận giao hàng
      </div>
      <motion.div
        initial={{ x: -50 }}
        animate={{ x: 0 }}
        className="pageContent-container"
        style={{ paddingBottom: 0 }}
      >
        <div className="confirmed-container container">
          <img className="confirmed-img" src={yayMascot} alt="" />
          <h1 className="confirmed-header">Xác nhận thành công</h1>
          <p className="confirmed-content">
            TAPTAP đã nhận được thông tin cúa bạn nha. Nếu có thay đổi về thông
            tin giao hàng, vui lòng gọi TAPTAP ngay nhé!
          </p>
          <button
            onClick={() => navigate("/history")}
            className="confirmed-primaryBtn primary-btn"
          >
            Xem lịch sử giao hàng
          </button>
          <a href="tel:+842873046446" className="confirmed-hotline">
            Gọi TAPTAP ngay
          </a>
        </div>
      </motion.div>
    </>
  );
};

export default ConfirmedPage;
