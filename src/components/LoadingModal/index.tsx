import CustomModal from "../CustomModal";
import walkingMascot from "../../core/assets/images/walking-mascot.png";

import "./LoadingModal.scss";

type Props = {
  isLoadingModalOpen: boolean;
  setIsLoadingModalOpen: any;
};

const LoadingModal = ({ isLoadingModalOpen }: Props) => {
  return (
    <CustomModal isModalOpen={isLoadingModalOpen} className="loadingModal">
      <img src={walkingMascot} alt="" />
      <p>Chờ xíu nha ...</p>
    </CustomModal>
  );
};

export default LoadingModal;
