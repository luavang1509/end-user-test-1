import { Key } from "antd/lib/table/interface";
import { Voucher } from "../../types";
import defaultMerchandise from "../../core/assets/images/default-merchandise.png";
import "./VouchersList.scss";

type Props = {
  vouchers: Voucher[];
};

const VouchersList = (props: Props) => {
  const { vouchers } = props;
  return (
    <div className="container">
      <div className="mb-2 title">
        <span>Quà sẽ giao</span>
      </div>
      {vouchers.map((voucher, index) => {
        const { rewardName, voucherCode, images } = voucher;
        return (
          <div key={index as Key} className="voucher-container mb-1">
            <div className="chosen_voucher-info">
              <img
                className="voucher-info_img"
                src={images ? images[0] : defaultMerchandise}
                alt="voucher-img"
              />
              <div className="voucher-info_text">
                <p className="voucher-info_text-name">{rewardName}</p>
                <p className="voucher-info_text-code">Mã: {voucherCode}</p>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default VouchersList;
