import React, { useRef, useEffect } from "react";
import { AnimatePresence, motion } from "framer-motion";

import "./CustomModal.scss";

type Props = {
  setIsModalOpen?: any;
  isModalOpen: boolean;
  children: React.ReactNode;
  style?: React.CSSProperties;
  className?: string;
};

const CustomModal = (props: Props) => {
  const { setIsModalOpen, isModalOpen, style, className, children } = props;
  const modalRef = useRef() as React.MutableRefObject<HTMLInputElement>;

  useEffect(() => {
    if (isModalOpen) document.body.style.overflow = "hidden";

    return () => {
      document.body.style.overflow = "auto";
    };
  }, [isModalOpen]);

  const handleOverlayClick = (e: React.MouseEvent<HTMLElement>) => {
    if (
      modalRef.current &&
      !modalRef.current.contains(e.target as HTMLInputElement)
    ) {
      setIsModalOpen(false);
    }
  };

  return (
    <AnimatePresence>
      {isModalOpen && (
        <motion.div
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
          transition={{ duration: 0.2 }}
          className="overlay"
          onClick={handleOverlayClick}
        >
          <div
            ref={modalRef}
            className={`modalContainer ${className}`}
            style={style}
          >
            {children}
          </div>
        </motion.div>
      )}
    </AnimatePresence>
  );
};

export default CustomModal;
