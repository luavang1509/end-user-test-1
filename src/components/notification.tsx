import { CloseOutlined } from "@ant-design/icons";
import { notification } from "antd";
import alertIcon from "../core/assets/icons/alert.png";

export const customNotification = (error: string) => {
  notification.open({
    key: "err",
    message: (
      <div className="notification">
        <img src={alertIcon} alt="alert" />
        <p>{error}</p>
        <CloseOutlined
          style={{ color: "#752d2d" }}
          onClick={() => notification.close("err")}
        />
      </div>
    ),
    placement: "top",
    duration: 2,
  });
};
