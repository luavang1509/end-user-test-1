import walkingMascot from "../../core/assets/images/walking-mascot.png";

import "./LoadingMascot.scss";

type Props = {};

const LoadingMascot = (props: Props) => {
  return (
    <div className="loading-screen" {...props}>
      <img src={walkingMascot} alt="" />
      <p>Chờ xíu nha ...</p>
    </div>
  );
};

export default LoadingMascot;
